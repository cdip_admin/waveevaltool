# Wave Eval Tool

A wave instrument intercomparison tool is to allow wave data collectors and instrument developers to easily compare wave spectral data from different sources, and view the differences between spectral data sets in a standardized form.
